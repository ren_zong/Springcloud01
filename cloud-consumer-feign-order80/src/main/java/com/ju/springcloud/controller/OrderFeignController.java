package com.ju.springcloud.controller;
import com.ju.springcloud.entitles.CommonResult;
import com.ju.springcloud.entitles.Payment;
import com.ju.springcloud.service.PaymentFeignService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
@RestController
public class OrderFeignController {
    @Resource
    private PaymentFeignService paymentFeignService;
    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult<Payment> getOrderById(@PathVariable("id") Long id){
        return paymentFeignService.selectById(id);
    }
}
