package com.ju.springcloud.config;
import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
public class FeignConfigLog {
    @Bean
    Logger.Level feignLoggerFull(){
        return Logger.Level.FULL;
    }
}
