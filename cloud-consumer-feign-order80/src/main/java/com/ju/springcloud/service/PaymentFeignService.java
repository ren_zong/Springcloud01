package com.ju.springcloud.service;

import com.ju.springcloud.entitles.CommonResult;
import com.ju.springcloud.entitles.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentFeignService {
    @GetMapping("/payment/get/{id}")
    public CommonResult selectById(@PathVariable("id") Long id);
}
