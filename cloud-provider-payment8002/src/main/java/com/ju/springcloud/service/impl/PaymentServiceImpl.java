package com.ju.springcloud.service.impl;

import com.ju.springcloud.dao.PaymentDao;
import com.ju.springcloud.entitles.Payment;
import com.ju.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Resource
    private PaymentDao paymentDao;
    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }
    @Override
    public Payment selectPaymentById(Long id) {
        return paymentDao.selectPaymentById(id);
    }
}
