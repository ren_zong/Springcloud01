package com.ju.springcloud.service;

import com.ju.springcloud.entitles.Payment;
import org.apache.ibatis.annotations.Param;

public interface PaymentService {
    public int create(Payment payment);
    public Payment selectPaymentById(@Param("id") Long id);
}
