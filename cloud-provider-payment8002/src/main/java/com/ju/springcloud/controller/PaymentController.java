package com.ju.springcloud.controller;
import com.ju.springcloud.entitles.CommonResult;
import com.ju.springcloud.entitles.Payment;
import com.ju.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
@Slf4j
@RestController
public class PaymentController {
    @Resource
    private PaymentService paymentService;
    /**
     * 设置端口号
     */
    @Value("${server.port}")
    private String serverPort;
    @PostMapping("/payment/create")
    public CommonResult create(@RequestBody Payment payment){
        int result = paymentService.create(payment);
        log.info("*********插入结果:{}",result);

        if (result > 0){
            return new CommonResult(200,"数据库插入成功,ServerPort"+serverPort,result);
        } else {
            return new CommonResult(404,"数据插入失败");
        }
    }
    @GetMapping("/payment/get/{id}")
    public CommonResult selectById(@PathVariable("id") Long id){
        Payment payment = paymentService.selectPaymentById(id);
        log.info("*********插入结果:{}",payment);
        if (payment != null){
            return new CommonResult(200,"数据库获取成功,ServerPort"+serverPort,payment);
        } else {
            return new CommonResult(404,"数据获取失败");
        }
    }
}
