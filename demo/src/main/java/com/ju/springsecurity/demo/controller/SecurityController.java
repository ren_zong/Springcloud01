package com.ju.springsecurity.demo.controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/security")
public class SecurityController {
        @GetMapping("/student/query")
        public String queryInfo01(){
            return "I am a student,My name is Eric!";
        }
        @GetMapping("/teacher/query")
        public String queryInfo02() {
            return "I am a teacher,My name is Thomas!";
        }
        @GetMapping("/admin/query")
        public String queryInfo03(){
            return "I am a administrator, My name is Obama!";
        }
}
