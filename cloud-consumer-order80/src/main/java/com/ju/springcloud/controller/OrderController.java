package com.ju.springcloud.controller;
import com.ju.springcloud.entitles.CommonResult;
import com.ju.springcloud.entitles.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import javax.annotation.Resource;
@Slf4j
@RestController
public class OrderController {
    public static final String PAYMENT_URI="http://CLOUD-PAYMENT-SERVICE";;
    @Resource
    private RestTemplate restTemplate;
    @GetMapping("/customer/payment/create")
    public CommonResult<Payment> createPayment(Payment payment){
        /* log.info("支付信息为:{}",payment);*/
        return restTemplate.postForObject(PAYMENT_URI+"/payment/create",payment,CommonResult.class);
        /*return restTemplate.postForEntity(PAYMENT_URI+"/payment/create",payment,CommonResult.class);*/
    }
    @ResponseBody
    @GetMapping("/customer/payment/get/{id}")
    public CommonResult<Payment> getPayment(@PathVariable("id") Long id){
        /*return restTemplate.getForEntity(PAYMENT_URI+"/payment/get/"+id,CommonResult.class);*/
        return restTemplate.getForObject(PAYMENT_URI+"/payment/get/"+id,CommonResult.class);
    }
    @GetMapping("/customer/payment/getEntity/{id}")
    public CommonResult<Payment> getPaymentEntity(@PathVariable("id") Long id){
        ResponseEntity<CommonResult> forEntity = restTemplate.getForEntity(PAYMENT_URI + "/payment/get/" + id, CommonResult.class);
        return forEntity.getBody();
        /* return restTemplate.getForObject(PAYMENT_URI+"/payment/get/"+id,CommonResult.class);*/
    }
}
