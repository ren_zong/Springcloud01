package com.ju.rules;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
public class MyRules01 {
    @Bean
    public IRule myRules001(){
        return new RandomRule();
    }
}
